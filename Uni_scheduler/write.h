//
// Created by AndyK on 15.10.2020.
//

#ifndef UNI_SCHEDULER_WRITE_H
#define UNI_SCHEDULER_WRITE_H

#include "Items/Event.h"
#include <vector>
#include <fstream>
#include <iostream>
class write {
private:
    std::vector<Event*> allEvents;
    std::string filename;
public:
    write();
    void writeToFile();
};


#endif //UNI_SCHEDULER_WRITE_H
