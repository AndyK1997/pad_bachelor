//
// Created by AndyK on 15.10.2020.
//

#ifndef UNI_SCHEDULER_USERINTERFACE_H
#define UNI_SCHEDULER_USERINTERFACE_H

#include <cstdlib>
#include <iostream>
#include "Items/Event.h"
#include <vector>
class Userinterface {
private:
    int choosed;
    bool programmState;
    std::vector<Event*> allEvents;

public:
    Userinterface(std::vector<Event*> List);
    void mainScreen();
    void manageInput(int choosed);
    void createEvent();
    void showNextEvent();
    void showAllEvents();
};


#endif //UNI_SCHEDULER_USERINTERFACE_H
