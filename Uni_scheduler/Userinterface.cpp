//
// Created by AndyK on 15.10.2020.
//

#include "Userinterface.h"

 Userinterface::Userinterface(std::vector<Event*> List){
    this->programmState= true;
    this->choosed=NULL;
    this->allEvents=List;
}


void Userinterface::mainScreen() {
    std::string c;
    while(this->programmState==true){
        std::cout<<"Bachelor 4 Semester H-da WS 2020/2021\n"<<std::endl;
        std::cout<<"01: Alle Events anzeigen"<<std::endl;
        std::cout<<"02: Neues Event Erstellen"<<std::endl;
        std::cout<<"03: nächstes Event Anzeigen"<<std::endl;
        std::cout<<"00: Programm beenden"<<std::endl;
        getline(std::cin,c);
        this->choosed=atoi(c.c_str());
        if(this->choosed>=0 && this->choosed<=3){
            manageInput(this->choosed);
            this->choosed=NULL;
        }
    }
}

void Userinterface::manageInput(int choosed) {
    switch (choosed){
        case 1 : showAllEvents();
        break;
        case 2 : createEvent();
        break;
        case 3 : showNextEvent();
        break;
        case 0 : this->programmState=false;
        break;
    }
}

void Userinterface::createEvent() {
    std::string name, link,time, rotation, duration;
    std::cout<<"Name: ";
    getline(std::cin,name);
    std::cout<<"Link: ";
    getline(std::cin,link);
    std::cout<<"Zeit: ";
    getline(std::cin,time);
    std::cout<<"turnus: 0=Einmalig, 7=Wöchentlich, 14=14-Taegig";
    getline(std::cin,rotation);
    std::cout<<"Dauer: 1-180min";
    getline(std::cin,duration);

    if((atoi(rotation.c_str())==0 ||atoi(rotation.c_str())==7 ||atoi(rotation.c_str())==14) && (atoi(duration.c_str())>=1 && atoi(duration.c_str())<=180)){
        Event *tmpEvent = new Event(name,link,time,atoi(rotation.c_str()),atoi(duration.c_str()));
        allEvents.push_back(tmpEvent);
        std::cout<<"Termin wurde erfolgreich eingetragen "<<std::endl;
    }else{
        std::cout<<"Ungültige eingabe"<<std::endl;
    }
}

void Userinterface::showNextEvent() {
    std::cout<<allEvents.front()->getName()<<" um "<<allEvents.front()->getTime()<<"Uhr. dauer "<<allEvents.front()->getDuration()<<std::endl;
}

void Userinterface::showAllEvents() {
    for(unsigned int x=0; x<allEvents.size(); x++){
        std::cout<<allEvents[x]->getName()<<" um "<<allEvents[x]->getTime()<<" Uhr, dauer"<<allEvents[x]->getDuration()<<std::endl;
    }
}

