//
// Created by AndyK on 15.10.2020.
//

#ifndef UNI_SCHEDULER_READ_H
#define UNI_SCHEDULER_READ_H


#include <string>
#include <vector>
#include "Items/Event.h"
class Read {
private:
    std::string filename;
    std::vector<Event*> allEvents;
public:
    Read();
    void readfFromFile();
};


#endif //UNI_SCHEDULER_READ_H
