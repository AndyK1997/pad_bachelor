//
// Created by AndyK on 15.10.2020.
//

#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include "Read.h"


void Read::readfFromFile() {
    allEvents.clear();
    std::string name, link, time, rotation, duration;
    std::fstream file;
    file.open(this->filename, std::ios::in);
    if(file.fail()==true){
        std::cerr<<"Datei konnte nicht geöffnet werden"<<std::endl;
    }else {
        while (file.peek() != EOF) {
            std::string line;
            getline(file, line);
            std::stringstream buffer(line);
            getline(buffer, name, '|');
            getline(buffer, link, '|');
            getline(buffer, time, '|');
            getline(buffer, rotation, '|');
            getline(buffer, duration, '|');
            Event *tmp = new Event(name, link, time, atoi(rotation.c_str()), atoi(duration.c_str()));
            allEvents.push_back(tmp);
        }
        file.close();
    }
}