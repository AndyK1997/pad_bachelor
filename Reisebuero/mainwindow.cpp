#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :                   //Konstruktor
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Reisebüro Upandway");             //Setzt die MainWindow beschriftung
    ui->table_allBookings->setColumnCount(4);               //Setzt die anzahl der Spalten der Tabelle
    ui->doubleSpinBox->setMaximum(100000);
    ui->groupBox_Details->hide();
    QStringList Spaltenueberschrift;
    Spaltenueberschrift<<"ID"<<"Preis"<<"Kunde"<<"ReiseNr";
    ui->table_allBookings->setHorizontalHeaderLabels(Spaltenueberschrift);  //Beschriftet die spallten der Tabelle
    ui->table_allBookings->verticalHeader()->setVisible(false);             //Versteckt die Tabelle
    ui->groupBox->setVisible(false);                                        //versteckt die Details der Buchungen
    ui->actionNeue_Buchung->setEnabled(false);                              //Deaktiviert den Button neue Buchung
    ui->actionspeichern_unter->setEnabled(false);

}
/**
 * @brief MainWindow::~MainWindow
 */

MainWindow::~MainWindow()                                   //Destruktor
{
    delete ui;
}
/**
 * @brief MainWindow::on_actionDatei_einlesen_triggered wenn der Button gedrückt wurden wird dieser ausgeführt und ein Text mit dem
 * Einleseergebmis in einem Pop up Angezeigt
 */

void MainWindow::on_actionDatei_einlesen_triggered()
{
    int Status=0;
   ui->actionDatei_einlesen->setEnabled(false);             //Button Datei Einlesen Deaktiviernen
   Status= Programm.readFile();                             //Einlesn von TravelAgency aufrufen
   if(Status==0){
   QMessageBox::information(this,"EinleseErgebnis",Programm.getMessageBoxText());   //Messagebox erstellen
   if(QMessageBox::Ok){
       UpdateTabel();                                       //Funkion Update Tabel aufrufen
       ui->groupBox->setVisible(true);                      //Details der Buchung sichtbar machen
   }

   ui->actionNeue_Buchung->setEnabled(true);                //Aktiviert den Button neue Buchung
   }
   else if(Status==-1){
       QMessageBox::critical(this,"Fehler beim Einlesen",QString::fromStdString(Programm.getException()));
       if(QMessageBox::Ok){

           ui->actionDatei_einlesen->setEnabled(true);
       }
   }
}
/**
 * @brief MainWindow::UpdateTabel Befüllt die Tabelle mit Daten
 */
void MainWindow::UpdateTabel(){
    KopieAllerBuchungen=Programm.getAllBookings();           //Kopiert die Referenz von dem Vektor @allBookings aus der Klasse TarvelAgency
    ui->table_allBookings->setRowCount(KopieAllerBuchungen.size()); //Setzt die anzahl der Zeilen der Tabele durch die Anzahl der Element  des vectors @allBookings

    for(unsigned int x=0; x<KopieAllerBuchungen.size(); x++){       //Läuft bis zum Letzten Element des Vectors @KopieAllerBuchungen

        string ID=to_string(KopieAllerBuchungen[x]->getID());       //Wandelt die id in typ String
                double Preis=KopieAllerBuchungen[x]->getPrice();      //
                long TravelID=KopieAllerBuchungen[x]->getTravID();  //Wandelt die @TravelID in string
                Travel* tmpReise=Programm.findTravel(TravelID);     //Temporäres element von einer reise die über die @TravelID gefunden wurden
                Customer* tmpKunde =Programm.findCustomer(tmpReise->getCustID());   //Temporäres elemsent vom typ customer

                        ui->table_allBookings->setItem(x,0,new QTableWidgetItem(QString::fromStdString(ID)));   //fügt in der Zeile x in der Splate 0 ein elemet vom Typ Qtabelwdgetitem hinzu
                        ui->table_allBookings->setItem(x,1,new QTableWidgetItem(QString::number(Preis)));       //fügt in der Zeile x in der Splate 1 ein elemet vom Typ Qtabelwdgetitem hinzu
                        ui->table_allBookings->setItem(x,2, new QTableWidgetItem(QString::fromStdString(tmpKunde->getName())));//fügt in der Zeile x in der Splate 2 ein elemet vom Typ Qtabelwdgetitem hinzu
                        ui->table_allBookings->setItem(x,3, new QTableWidgetItem(QString::fromStdString(to_string(TravelID))));//fügt in der Zeile x in der Splate 3 ein elemet vom Typ Qtabelwdgetitem hinzu

    }

}
/**
 * @brief MainWindow::on_table_allBookings_cellClicked Ermittelt das Datum der an und abreise und die ID der angeklickten Zelle der Tabele @param tabel_allBookings
 * @param row
 * @param column
 */

void MainWindow::on_table_allBookings_cellClicked(int row, int column)
{
    ui->line_Bnr->setText(ui->table_allBookings->item(row,0)->text());  //Schreibt die Buchungsnummer in die Ausgaben Zeile
    string Jahr=KopieAllerBuchungen[row]->getFromDate().substr(0,4);    //Zerlegt den String @fromDate in das Jahr nach schema yyyy
    string Monat=KopieAllerBuchungen[row]->getFromDate().substr(4,2);   //Zerlegt den String @fromDate in den Monat nach schema MM
    string Tag=KopieAllerBuchungen[row]->getFromDate().substr(6,2);     //Zerlegt den String @fromDate in den Tag nahc schema DD
    ui->Calendar_Von->setSelectedDate(QDate(stoi(Jahr),stoi(Monat),stoi(Tag)));//Setzt das Datum in dem Kalender Widget
     Jahr=KopieAllerBuchungen[row]->getToDate().substr(0,4);            //Zerlegt den String @toDate in das Jahr nach schema yyyy
     Monat=KopieAllerBuchungen[row]->getToDate().substr(4,2);           //Zerlegt den String @toDate in das Jahr nach schema MM
    Tag=KopieAllerBuchungen[row]->getToDate().substr(6,2);              //Zerlegt den String @toDate in das Jahr nach schema DD
   ui->Calendar_Bis->setSelectedDate(QDate(stoi(Jahr),stoi(Monat),stoi(Tag)));//Setzt das Datum in dem Kalender Widget
   ui->doubleSpinBox->setValue(KopieAllerBuchungen[row]->getPrice());
   ui->spinBox->setValue(KopieAllerBuchungen[row]->getTravID());
   Travel* tmp=Programm.findTravel(KopieAllerBuchungen[row]->getTravID());
   Customer* Tmp=Programm.findCustomer(tmp->getCustID());
   ui->lineEdit->setText(QString::fromStdString(Tmp->getName()));
   if(FlightBooking* tmp =dynamic_cast<FlightBooking*>(KopieAllerBuchungen[row])){
       ui->groupBox_Details->setTitle("Flugbuchung");
       ui->label_Detail1->setText("AbflubFlughafen: ");
       ui->label_Detail2->setText("Zielflughafen: ");
       ui->label_Detail3->setText("Sitzpreferenz: ");
       ui->text_Detail1->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft1()));
       ui->text_Detail2->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft2()));
       if(KopieAllerBuchungen[row]->Eigenschaft4()=="W"){
           ui->text_Deatil3->setText("Fenster");
       }else{
           ui->text_Deatil3->setText("Gang");
       }
       ui->groupBox_Details->show();
       ui->text_Deatil3->show();


   }
   else if(RentalCarReservation* tmp =dynamic_cast<RentalCarReservation*>(KopieAllerBuchungen[row])){
       ui->groupBox_Details->setTitle("MietwagenReservierung");
       ui->label_Detail1->setText("Abholstation: ");
       ui->label_Detail2->setText("Rückgabestation: ");
       ui->label_Detail3->setText("Versicherung: ");
       ui->text_Detail1->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft1()));
       ui->text_Detail2->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft2()));
       ui->text_Deatil3->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft4()));
       ui->groupBox_Details->show();
       ui->text_Deatil3->show();



   }
   else if(HotelBooking* tmp= dynamic_cast<HotelBooking*>(KopieAllerBuchungen[row])){
       ui->groupBox_Details->setTitle("HotelBuchung");
       ui->label_Detail1->setText("Name: ");
       ui->label_Detail2->setText("Stadt: ");
       if(KopieAllerBuchungen[row]->Eigenschaft4()=="1"){
           ui->label_Detail3->setText("Raucherzimmer");
       }else{
           ui->label_Detail3->setText("Nichtraucherzimmer");
       }

       ui->text_Detail1->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft1()));
       ui->text_Detail2->setText(QString::fromStdString(KopieAllerBuchungen[row]->Eigenschaft2()));
       ui->text_Deatil3->hide();
       ui->groupBox_Details->show();

   }


}
/**
 * @brief MainWindow::on_actionNeue_Buchung_triggered Öffnet das neue Buchungs Fenster, versteckt das Hauptfenster
 *  und löscht es anschliesend nach erfolgreicher Buchung wieder vom Heap
 */

void MainWindow::on_actionNeue_Buchung_triggered()
{
    this->hide();                                                       //Versteckz das MainWindow
    popUp = new Dialog_NeueBuchung(this);                               //Erzeugt ein Neues PopUp
    popUp->show();                                                      //Zeigt das neue PopUP an
    popUp->exec();
                                                                        //Wartet bis das PopUp wieder Geschlossen wurden ==System(PAUSE)
    try{
    char Typ=popUp->getTyp();                                           //Holt die Daten aus den einzelnen popUP eingabeFelder
    double preis=popUp->getPrice();                                     //*
    long ReiseID=popUp->getTravelID();                                  //*
    string Start=popUp->getStartDatum();                                //*
    string ende=popUp->getEndDatum();                                   //*
    vector<string> Details=popUp->getDetails();                         //*
    Details.push_back("Template");

   if((Typ==NULL||preis==NULL||ReiseID==NULL||Start==""||ende==""||Details[0]==""||Details[1]==""||Details[2]==""||Details[3]=="")&&Details[0]!="Abbruch"){
        throw string("Keine gültige eingabe bitte erneut Buchen");
    }

   else if(Details[0]=="Abbruch"){
       this->show();                                                       //Zeigt das MainWindow wieder an
       delete popUp;                                                       //löscht das PopUp
       popUp=nullptr;
    }else{
     Details.pop_back();
    Programm.createBooking(Typ,preis,Start,ende,ReiseID,Details);       //Schreibt die Daten in den Vector @allBookings(erzeugt einen neue Buchung)
    UpdateTabel();                                                      //Ruft die Update Funton der Tabelle
    this->show();                                                       //Zeigt das MainWindow wieder an
    delete popUp;                                                       //löscht das PopUp
    popUp=nullptr;
     ui->actionspeichern_unter->setEnabled(true);
    }
    }catch(string x){
        this->show();                                                       //Zeigt das MainWindow wieder an
        delete popUp;                                                       //löscht das PopUp
        popUp=nullptr;
        QMessageBox::critical(this,"Fehler beim Einlesen",QString::fromStdString(x));
        if(QMessageBox::Ok){
            ui->actionNeue_Buchung->trigger();
        }
    }

}

void MainWindow::on_actionspeichern_unter_triggered()
{
    SavePopUP=new dialog_save(this);                                    //erstellt ein neues popup
    SavePopUP->show();                                                  //zeigt das neue popup an
    SavePopUP->exec();                                                  //wartet bis das neue popup ausgeführt oder geschlossen wurde
    string Pfad=SavePopUP->getPfad();                                   //Holt den pfad vom popup
    Programm.SaveBookings(Pfad);                                        //übergibt der funktion savebookings den pfad

    delete SavePopUP;                                                   //löscht das popup
    SavePopUP=nullptr;                                                  //setzt den popup pointer auf nullptr
}
