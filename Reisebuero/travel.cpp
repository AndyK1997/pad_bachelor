#include "travel.h"
#include "Travel.h"


Travel::Travel(long id, long customerID)
{
    this->id = id;
    this->customerID = customerID;
}


Travel::~Travel()
{
}

void Travel::addBooking(Booking* booking) {
    travelBookings.push_back(booking);
}

long Travel::getID() {
    return id;
}

long Travel::getCustID() {
    return customerID;
}

int Travel::getCountOfBookungs() {
    return travelBookings.size()-1;
}
