#include "dialog_save.h"
#include "ui_dialog_save.h"

dialog_save::dialog_save(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_save)
{
    ui->setupUi(this);
    this->setWindowTitle("Speichern unter");
    ui->lineEdit->setText("C:\\Users\\AndyK\\Desktop\\");
}
/**
 * @brief dialog_save::~dialog_save
 */
dialog_save::~dialog_save()
{
    delete ui;
}
/**
 * @brief dialog_save::getPfad
 * @return
 */
std::string dialog_save::getPfad() const
{
    return Pfad;
}
/**
 * @brief dialog_save::on_buttonBox_accepted
 */
void dialog_save::on_buttonBox_accepted()
{
    Pfad=ui->lineEdit->text().toStdString();
}

void dialog_save::on_buttonBox_rejected()
{

}
