#include "customer.h"
#include <iostream>


Customer::Customer(long id , string name)
{
    this->id = id;
    this->name = name;
}


Customer::~Customer()
{
}

void Customer::addTravel(Travel* travel) {
    travelListe.push_back(travel);
}

long Customer::getID() {
    return id;
}

int Customer::getCountofTravel() {
    return travelListe.size();
}

string Customer::getName() {
    return name;
}
