#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <QtTest/QTest>
#include "travelagency.h"
class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = nullptr);

signals:

public slots:

private slots:
    void TestFunktion();
};

#endif // TEST_H
