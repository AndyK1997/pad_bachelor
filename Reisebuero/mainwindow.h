#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "travelagency.h"
#include <QStringList>
#include <vector>
#include "dialog_neuebuchung.h"
#include "dialog_save.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void UpdateTabel();

private slots:
    void on_actionDatei_einlesen_triggered();

    void on_table_allBookings_cellClicked(int row, int column);

    void on_actionNeue_Buchung_triggered();

    void on_actionspeichern_unter_triggered();

private:
    Ui::MainWindow *ui;
    TravelAgency Programm;
    vector<Booking*> KopieAllerBuchungen;
    Dialog_NeueBuchung *popUp;
    dialog_save *SavePopUP;
};

#endif // MAINWINDOW_H
