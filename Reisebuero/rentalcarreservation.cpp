#include "rentalcarreservation.h"
#include "RentalCarReservation.h"


RentalCarReservation::RentalCarReservation(long id, double price, long travelID, string fromDate,string toDate, string pickuplocation, string returnLocation, string company, string InsuranceType)
{
    this->id = id;
    this->price = price;
    this->travelID = travelID;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->pickupLocation = pickuplocation;
    this->returnLocation = returnLocation;
    this->company = company;
    this->insuranceType=InsuranceType;
}


RentalCarReservation::~RentalCarReservation()
{
}

string RentalCarReservation::Eigenschaft1() {
    return pickupLocation;
}

string RentalCarReservation::Eigenschaft2() {
    return returnLocation;
}

string RentalCarReservation::Eigenschaft3() {
    return company;
}

string RentalCarReservation::Eigenschaft4(){
    return insuranceType;
}
