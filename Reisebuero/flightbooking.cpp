#include "flightbooking.h"

FlightBooking::FlightBooking(long id, double price, long travelID, string fromDate, string toDate, string fromDest, string toDest, string airline, char SeatPref)
{
    this->id = id;
    this->price = price;
    this->travelID = travelID;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->fromDest = fromDest;
    this->toDest = toDest;
    this->airline = airline;
    this->seatPref=SeatPref;
}


FlightBooking::~FlightBooking()
{
}
string FlightBooking::Eigenschaft1() {
    return fromDest;
}

string FlightBooking::Eigenschaft2() {
    return toDest;
}

string FlightBooking::Eigenschaft3() {
    return airline;
}

string FlightBooking::Eigenschaft4(){
    if(seatPref=='W'){
        return "W";
    }
    else if(seatPref=='A'){
        return "A";
    }
    else
        return "Null";
}
