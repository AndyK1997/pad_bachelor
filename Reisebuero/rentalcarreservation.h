#ifndef RENTALCARRESERVATION_H
#define RENTALCARRESERVATION_H

#include "Booking.h"
using namespace std;
class RentalCarReservation :public Booking
{
public:
    RentalCarReservation(long id, double price, long travelID, string fromDate,string toDate, string pickuplocation, string returnLocation, string company, string InsuranceType);
    ~RentalCarReservation();
    virtual string Eigenschaft1();
    virtual string Eigenschaft2();
    virtual string Eigenschaft3();
    virtual string Eigenschaft4();
private:
    string pickupLocation;
    string returnLocation;
    string company;
    string insuranceType;
};



#endif // RENTALCARRESERVATION_H
