#ifndef DIALOG_SAVE_H
#define DIALOG_SAVE_H

#include <QDialog>
#include <string>
namespace Ui {
class dialog_save;
}

class dialog_save : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_save(QWidget *parent = nullptr);
    ~dialog_save();

    std::string getPfad() const;

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::dialog_save *ui;
    std::string Pfad;
};

#endif // DIALOG_SAVE_H
