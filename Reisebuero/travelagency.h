#ifndef TRAVELAGENCY_H
#define TRAVELAGENCY_H
#include "Booking.h"
#include "Customer.h"
#include "Travel.h"
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include "FlightBooking.h"
#include "RentalCarReservation.h"
#include "HotelBooking.h"
#include <exception>
#include <QString>
using namespace std;
class TravelAgency
{
public:
    TravelAgency();
    ~TravelAgency();
    int readFile();
    Booking* findBooking(long id);
    Travel* findTravel(long id);
    Customer* findCustomer(long id);
    QString getMessageBoxText();
    void MaxPrice();
    //Aufgabe 3
    long NewBookingID();
    int createBooking(char type, double price, string start, string end, long travelId, vector<string> bookingDetails);
    vector<Booking *> getAllBookings() const;
    void setAllBookings(const vector<Booking *> &value);
    string getException() const;
    void SaveBookings(string Pfad);
    int getTest1(char TestTyp);
    int getTest2();
    int getTest3();

private:
    vector<Booking*> allBookings;
    vector<Customer*> allCoustomer;
    vector<Travel*> allTravels;
    int cntFlight, cntRental, cntHotel;
    string Exception;
    float Gesammtwert;

};



#endif // TRAVELAGENCY_H
