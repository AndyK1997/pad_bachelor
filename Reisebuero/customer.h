#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "Travel.h"
#include <vector>
using namespace std;
class Customer
{
public:
    Customer(long id, string name);
    ~Customer();
    void addTravel(Travel* travel);
    long getID();
    int getCountofTravel();
    string getName();
private:
    long id;
    string name;
    vector<Travel*> travelListe;
};


#endif // CUSTOMER_H
