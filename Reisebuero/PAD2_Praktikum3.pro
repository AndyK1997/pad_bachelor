#-------------------------------------------------
#
# Project created by QtCreator 2019-05-09T12:17:36
#
#-------------------------------------------------

QT       += core gui
QT      +=testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PAD2_Praktikum3
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        booking.cpp \
        customer.cpp \
        dialog_neuebuchung.cpp \
        flightbooking.cpp \
        hotelbooking.cpp \
        main.cpp \
        mainwindow.cpp \
        rentalcarreservation.cpp \
    test.cpp \
        travel.cpp \
        travelagency.cpp \
    dialog_save.cpp

HEADERS += \
        booking.h \
        customer.h \
        dialog_neuebuchung.h \
        flightbooking.h \
        hotelbooking.h \
        mainwindow.h \
        rentalcarreservation.h \
    test.h \
        travel.h \
        travelagency.h \
    dialog_save.h

FORMS += \
        dialog_neuebuchung.ui \
        mainwindow.ui \
    dialog_save.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
