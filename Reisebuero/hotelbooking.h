#ifndef HOTELBOOKING_H
#define HOTELBOOKING_H
#include "Booking.h"
using namespace std;
class HotelBooking :public Booking
{
public:
    HotelBooking(long id, double price, long travelID, string fromDate, string toDate, string hotel, string town, bool smoke);
    ~HotelBooking();
    virtual string Eigenschaft1();
    virtual string Eigenschaft2();
    virtual string Eigenschaft4();
private:
    string hotel;
    string town;
    bool smoke;
};



#endif // HOTELBOOKING_H
