#include "travelagency.h"
#include "TravelAgency.h"


TravelAgency::TravelAgency()    //Konstruktor von Travel Agency
{
    this->cntFlight = 0;
    this->cntHotel = 0;
    this->cntRental = 0;
}
/*
*/
TravelAgency::~TravelAgency()
{
}
/*
*/
int  TravelAgency::readFile() {

    ifstream Datei;
    Datei.open("C:\\Users\\AndyK\\Desktop\\bookings_praktikum4.txt");   //Öffnet die  Datei die auf dem Pfad liegt

    if (Datei.fail()==1) {
        cerr << "bookings2 wurde nicht gefunden" << endl;
    }
    else {
        string line, strTMP;

        while (Datei.peek()!=EOF) {
            char Typ=NULL;
            char Zeichen=NULL;
            long id=NULL;
            double Price=NULL;
            string fromDate="leer";
            string toDate="leer";
            long travelID=NULL;
            long customerID=NULL;
            string name="leer";
            try{
            getline(Datei, line);				//Liest Zeilen aus der Datei in @string Line
            stringstream buffer(line);			//In den Puffer wird die Zeile gelesen
            string typString;


            getline(buffer, typString, '|'); Typ = typString[0];	//Aus den Puffer wird der Typ Gelesen
            getline(buffer, strTMP, '|'); id = atol(strTMP.c_str());	//Aus den Puffer wird die BuchungsID gelesen und in long gewandelt
            getline(buffer, strTMP, '|'); Price = atof(strTMP.c_str());	//Aus dem Puffer wird der Preis gelesen un in double gewandelt
            getline(buffer, strTMP, '|'); fromDate = strTMP;			//Aus dem Puffer wird das anfangs datum  gelesen
            getline(buffer, strTMP, '|'); toDate = strTMP;
            getline(buffer, strTMP, '|'); travelID = atol(strTMP.c_str());//Aus dem Puffer wird die ReiseID gelsesen und in long gewandelt
            getline(buffer, strTMP, '|'); customerID =atol(strTMP.c_str());//Aus dem Puffer wird die KundenIDgelsen und in long gewandelt
            getline(buffer, strTMP, '|'); name = strTMP;				//Aus dem Puffer wird der Kundne Name gelesen

            if(Typ!='F'&&Typ!='R'&&Typ!='H'){
                throw string("Ungültiger BuchungsTyp ");
            }
            switch (Typ) {
            case 'F': {

                string fromDest="leer", toDest="leer", airline="leer", SeatPref="leer";
                getline(buffer, fromDest, '|'); getline(buffer, toDest, '|'); getline(buffer, airline,'|'); getline(buffer, SeatPref,'|');//Lsen von @fromDest, @toDest und @airline aus dem Puffer
                char seatPref;
                seatPref=SeatPref[0];
                if(Typ==NULL||id==NULL||Price==NULL||fromDate=="leer"||toDate=="leer"||travelID==NULL||customerID==NULL||fromDest=="leer"||name=="leer"||toDest=="leer"||airline=="leer"||SeatPref=="leer"){
                    throw string("Flugbuchung");
                }
                FlightBooking* tempBooking = new FlightBooking(id, Price, travelID, fromDate, toDate, fromDest, toDest, airline,seatPref); //Anlegen einer Temporären Flugbuchung

                allBookings.push_back(tempBooking);                      //Abspeicherung der Temporäen buchung im Vector @allBookings
                cntFlight++;											//Erhöhung des Counters der die Anzahl der Flugbuchungen Zählt	 @cntFlight
                Travel* tmpTravel = findTravel(travelID);				//Kopierung von dem Pointe der find Travel liefert

                if (tmpTravel != nullptr) {								//Wenn der Wert ungleich NULL ist wird die Buchung nur dem Vector @Buchungen hinzugefügt
                    tmpTravel->addBooking(tempBooking);
                }
                else {													//Wenn nicht dann wird dem bestehenden element die Buchung hinzugefügt
                    allTravels.push_back(new Travel(travelID, customerID));
                    allTravels.back()->addBooking(tempBooking);
                }

                Customer* tmpCustomer = findCustomer(customerID);		//Kopierung von dem Pointer den findCustomer  liefert
                if (tmpCustomer != nullptr) {								//Wenn der Wert gleich NULL ist dann wird ein neues Element angelegt
                    tmpCustomer->addTravel(tmpTravel);
                }
                else {													//Wenn nicht dann wird dem bestehenden element die Buchung hinzugefügt
                    allCoustomer.push_back(new Customer(customerID, name));
                    allCoustomer.back()->addTravel(tmpTravel);
                }

                break;
            }

            case 'R': {

                string pickuplocation="leer", returnLocation="leer", company="leer", insuranceTyp="leer";
                getline(buffer, pickuplocation, '|'); getline(buffer, returnLocation, '|'); getline(buffer, company, '|'); getline(buffer, insuranceTyp,'|');	//Lesen von @pickupLocation, @returnLocation @company aus dem Puffer
                if(Typ==NULL||id==NULL||Price==NULL||fromDate=="leer"||toDate=="leer"||travelID==NULL||customerID==NULL||pickuplocation=="leer"||name=="leer"||returnLocation=="leer"||company=="leer"||insuranceTyp=="leer"){
                    throw string("Mietwagenreservierung");
                }
                RentalCarReservation* tmpBooking = new RentalCarReservation(id, Price, travelID, fromDate, toDate, pickuplocation, returnLocation, company, insuranceTyp); //Anlegen der temporären Autoreservierung
                allBookings.push_back(tmpBooking);						//Abspeicherung der Autoreservierung im Vector @allBookings
                cntRental++;											//Erhöhung des Counters der die Autoreservierungen Zählt @cntRental

                Travel* tmpTravel = findTravel(travelID);

                if (tmpTravel != nullptr) {								//Wenn der wert ungleich nullptr ist wird die buchungh der dazugehörigen reisen übergeben
                    tmpTravel->addBooking(tmpBooking);
                }
                else {													//Wenn der wert gleich nullptr ist wird eine neue Reise angelegt und danach die Buchung dieser Reise hinzugefügt
                    allTravels.push_back(new Travel(travelID, customerID));
                    allTravels.back()->addBooking(tmpBooking);
                }

                Customer* tmpCustomer = findCustomer(customerID);

                if (tmpCustomer != nullptr) {
                    tmpCustomer->addTravel(tmpTravel);
                }
                else {
                    allCoustomer.push_back(new Customer(customerID, name));
                    allCoustomer.back()->addTravel(tmpTravel);
                }
                break;
            }

            case 'H': {

                string hotel="leer", town="leer", Smoke="leer";

                getline(buffer, hotel, '|'); getline(buffer, town, '|'); getline(buffer, Smoke,'|');
                stringstream tmp(Smoke);
                bool smoke;
                tmp>>smoke;
                if(Typ==NULL||id==NULL||Price==NULL||fromDate=="leer"||toDate=="leer"||travelID==NULL||customerID==NULL||hotel=="leer"||name=="leer"||town=="leer"||Smoke=="leer"){
                    throw  string("HotelBuchung");
                }
                HotelBooking* tmpBooking = new HotelBooking(id, Price, travelID, fromDate, toDate, hotel, town, smoke);
                allBookings.push_back(tmpBooking);
                cntHotel++;

                Travel* tmpTravel = findTravel(travelID);

                if (tmpTravel != nullptr) {
                    tmpTravel->addBooking(tmpBooking);
                }
                else {
                    allTravels.push_back(new Travel(travelID, customerID));
                    allTravels.back()->addBooking(tmpBooking);
                }

                Customer* tmpCustomer = findCustomer(customerID);

                if (tmpCustomer != nullptr) {
                    tmpCustomer->addTravel(tmpTravel);
                }
                else {
                    allCoustomer.push_back(new Customer(customerID, name));
                    allCoustomer.back()->addTravel(tmpTravel);
                }

                break;
            }
            default: cout << "ERROR!!!!" << endl;
                break;
            }

            }catch(string  x){
                this->Exception=x+"  In der Zeile"+to_string(allBookings.size())+"Ist ein Fehler aufgetreten";
                allBookings.clear();
                allCoustomer.clear();
                allTravels.clear();
                Datei.close();
                return -1;
            }catch(...){
                this->Exception="In der Zeile"+to_string(allBookings.size())+"Ist ein Fehler aufgetreten";
                allBookings.clear();
                allCoustomer.clear();
                allTravels.clear();
                Datei.close();
                return -1;
            }

        }
        Datei.close();													//Schließt die Datei
    }
    return 0;
}
/*
*/
Booking* TravelAgency::findBooking(long id) {							//Sucht die Buchung mit der @id aus dem Vector @allBoojings
    bool found = false;

    for (unsigned int x = 0; x < allBookings.size(); x++) {
        if (id == allBookings[x]->getID()) {							//Wenn die Buchung gefunden wurde giebt die Funktion die Buchungs Referenz zurück @allBookings[x
            found = true;
            return allBookings[x];
        }
    }

    if (found == false) {												//Wenn die Buchung nicht existiert gibt die Funktion den @nullptr zurück
        return nullptr;
    }
}
/*
*/
Customer* TravelAgency::findCustomer(long id) {							//Sucht den Kunden mit der @id aus dem Vector @allCoustomer

    bool found = false;

    for (unsigned int x = 0; x < allCoustomer.size(); x++) {
        if (id == allCoustomer[x]->getID()) {							//Wenn der Kunde gefunden wurden gibt die Funktion die Referenz auf den KUnden @allCoustomer[x
            return allCoustomer[x];
            found = true;
        }
    }

    if (found == false) {												//Wenn der Kunde nicht existiert gibt die funktion den nullptr zurück
        return nullptr;
    }
}
/*
*/
Travel* TravelAgency::findTravel(long id) {								//Sucht die Reise mit der @id aus dem Vector @allTravels

    bool found = false;

    for (unsigned int x = 0; x < allTravels.size(); x++) {
        if (id == allTravels[x]->getID()) {								//Wenn die Reise gefunden wurde gibt die Funktion die Referenz der Reise zurück @allTravels[x]

            return allTravels[x];
            found = true;
        }
    }

    if (found == false) {												//Wenn die Reise nicht existiert gibt die Funktion den @nullptr zurück
        return nullptr;
    }
}
/*
*/
QString TravelAgency::getMessageBoxText() {											//Konsolen Ausgabe des Einlese ergebnisses

    MaxPrice();
    QString Text;
    QString Preis= QString::number(Gesammtwert);
    QString AnzahlBuchungen=QString::number( allBookings.size());
    QString AnzahlKunden= QString::number(allCoustomer.size());
    QString  AnzahlReisen=QString::number( allTravels.size());
    Text="Es wurden "+AnzahlBuchungen+" Buchungen, "+AnzahlKunden+" Kunden, "+AnzahlReisen+"Reisen im Gesammtwert von"+Preis+"Eingelesen";
    return Text;

}
/*
*/
void TravelAgency::MaxPrice() {										//Ermittelt den Gesammtpreis aller Buchungen und Speichert den Wert in der VAriable @Gesammtwert

    Gesammtwert = 0;

    for (unsigned int x = 0; x < allBookings.size(); x++) {
        Gesammtwert += allBookings[x]->getPrice();
    }
}

/*
Aufgabe 3
*/

long TravelAgency::NewBookingID() {								//Sucht die Höchste Buchungs ID und gibt diese id+1 zurück

    long NewID = allBookings.front()->getID();

    for (int x = 0; x < allBookings.size();  x++) {
        if (allBookings[x]->getID() > NewID) {
            NewID = allBookings[x]->getID();
        }
    }

    return NewID+1;
}

/*
*/
int TravelAgency::createBooking(char type, double price, string start, string end, long travelID, vector<string> bookingDetails) {	//Erstellt einen Neue Buchung falls die Reise Bereits existiert

    Travel* tmpTravel = findTravel(travelID);

    if (tmpTravel != nullptr) {

        switch (type) {

        case 'F': {
            char seatPref=bookingDetails[3].front();
            FlightBooking* tmpBooking = new FlightBooking(NewBookingID(), price, travelID, start, end, bookingDetails[0], bookingDetails[1], bookingDetails[2],seatPref);
            allBookings.push_back(tmpBooking);
            tmpTravel->addBooking(tmpBooking);

            Customer* tmp = findCustomer(travelID);

            if (tmp != nullptr) {
                tmp->addTravel(tmpTravel);
            }

            cntFlight++;

            break;
        }
        case 'R': {
            RentalCarReservation* tmpBooking = new RentalCarReservation(NewBookingID(), price, travelID, start, end, bookingDetails[0], bookingDetails[1], bookingDetails[2],bookingDetails[3]);

            allBookings.push_back(tmpBooking);
            tmpTravel->addBooking(tmpBooking);

            Customer* tmp = findCustomer(travelID);

            if (tmp != nullptr) {
                tmp->addTravel(tmpTravel);
            }

            cntRental++;

            break;
        }

        case 'H': {
            stringstream Tmp(bookingDetails[2]);
            bool smoke=1;
            Tmp>>smoke;
            HotelBooking* tmpBooking = new HotelBooking(NewBookingID(), price, travelID, start, end, bookingDetails[0], bookingDetails[1],smoke);

            allBookings.push_back(tmpBooking);
            tmpTravel->addBooking(tmpBooking);

            Customer* tmp = findCustomer(travelID);

            if (tmp != nullptr) {
                tmp->addTravel(tmpTravel);
            }

            cntHotel++;

            break;
        }

        default: cout <<"ERROR Ungültiger Typ" << endl;
            break;
        }

        cout << endl << endl;
        cout << "Die Reise mit Der ID " << allBookings.back()->getID() << " wurde angelegt." << endl;
        cout << endl << endl;
    }
    else {

        return -1;
    }
}
/**
 * @brief TravelAgency::getAllBookings
 * @return
 */
vector<Booking *> TravelAgency::getAllBookings() const
{
    return allBookings;
}
/**
 * @brief TravelAgency::setAllBookings
 * @param value
 */
void TravelAgency::setAllBookings(const vector<Booking *> &value)
{
    allBookings = value;
}
/**
 * @brief TravelAgency::getException
 * @return
 */
string TravelAgency::getException() const
{
    return Exception;
}
/**
 * @brief TravelAgency::SaveBookings Speichert die DAten in einer neuen Txt datei
 * @param Pfad
 */
void TravelAgency::SaveBookings(string Pfad){
    ofstream NeueDatei;
    NeueDatei.open(Pfad);

    if(NeueDatei.fail()==1){
        cerr<<"Datei konnte nicht erstellt werden"<<endl;
    }
    else{

        for(int x=0; x<allBookings.size(); x++){
            stringstream output;
            if(FlightBooking* tmp=dynamic_cast<FlightBooking*>(allBookings[x])){        //überprüft ob der buchungtyp vom typ Flightbooking ist
                Travel* SpeicherTravel=findTravel(allBookings[x]->getTravID());         //erstellt eine temporäre referenz auf die reise zu der gefundenen buchung
                Customer* SpeicherCustomer=findCustomer(SpeicherTravel->getCustID());   //erstellt eine temporäre referenz auf den kunden über die ermittelte reise
                output<<"F|"<<allBookings[x]->getID()<<"|"<<allBookings[x]->getPrice()<<"|"<<allBookings[x]->getFromDate()<<"|"<<allBookings[x]->getToDate()<<"|"<<allBookings[x]->getTravID()<<"|"<<SpeicherCustomer->getID()<<"|"<<SpeicherCustomer->getName()<<"|"<<allBookings[x]->Eigenschaft1()<<"|"<<allBookings[x]->Eigenschaft2()<<"|"<<allBookings[x]->Eigenschaft3()<<"|"<<allBookings[x]->Eigenschaft4();
                 NeueDatei<<output.str()<<"\r";                                         //wandelt den stringstream in einen string und schreibt diesen in die Datei
            }
            else if(RentalCarReservation* tmp=dynamic_cast<RentalCarReservation*>(allBookings[x])){
                Travel* SpeicherTravel=findTravel(allBookings[x]->getTravID());
                Customer* SpeicherCustomer=findCustomer(SpeicherTravel->getCustID());
                output<<"R|"<<allBookings[x]->getID()<<"|"<<allBookings[x]->getPrice()<<"|"<<allBookings[x]->getFromDate()<<"|"<<allBookings[x]->getToDate()<<"|"<<allBookings[x]->getTravID()<<"|"<<SpeicherCustomer->getID()<<"|"<<SpeicherCustomer->getName()<<"|"<<allBookings[x]->Eigenschaft1()<<"|"<<allBookings[x]->Eigenschaft2()<<"|"<<allBookings[x]->Eigenschaft3()<<"|"<<allBookings[x]->Eigenschaft4();
                  NeueDatei<<output.str()<<"\r";
            }
            else if(HotelBooking* tmp=dynamic_cast<HotelBooking*>(allBookings[x])){
                Travel* SpeicherTravel=findTravel(allBookings[x]->getTravID());
                Customer* SpeicherCustomer=findCustomer(SpeicherTravel->getCustID());
                output<<"H|"<<allBookings[x]->getID()<<"|"<<allBookings[x]->getPrice()<<"|"<<allBookings[x]->getFromDate()<<"|"<<allBookings[x]->getToDate()<<"|"<<allBookings[x]->getTravID()<<"|"<<SpeicherCustomer->getID()<<"|"<<SpeicherCustomer->getName()<<"|"<<allBookings[x]->Eigenschaft1()<<"|"<<allBookings[x]->Eigenschaft2()<<"|"<<allBookings[x]->Eigenschaft4();
                  NeueDatei<<output.str()<<"\r";
            }
        }
    }
}
/**
 * @brief TravelAgency::getTest1 funktion Für den Unit Test
 * @param TestTyp
 * @return
 */
int TravelAgency::getTest1(char TestTyp){
     int cnt =0;
     for(int x=0; x<allBookings.size(); x++){
         switch(TestTyp){
         case 'R':{
             if(RentalCarReservation* tmp=dynamic_cast<RentalCarReservation*>(allBookings[x])){
                 if(tmp->Eigenschaft3()=="Avis"){
                     cnt++;
                 }
             }
             break;
         }
         case 'F':{
             if(FlightBooking* tmp=dynamic_cast<FlightBooking*>(allBookings[x])){
                 if(tmp->Eigenschaft3()=="United Airlines"){
                     cnt++;
                 }
             }
            break;
         }
         case 'T':{
                 if(allBookings[x]->getPrice()>=1000){
                     cnt++;
                }

             break;
         }
         }
     }
     return cnt;
}


