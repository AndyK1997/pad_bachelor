#ifndef TRAVEL_H
#define TRAVEL_H
#include <vector>
#include "Booking.h"
class Travel
{
public:
    Travel(long id, long customerID);
    ~Travel();
    void addBooking(Booking* booking);
    long getID();
    long getCustID();
    int getCountOfBookungs();
private:
    long id;
    long customerID;
    vector<Booking*> travelBookings;
};



#endif // TRAVEL_H
