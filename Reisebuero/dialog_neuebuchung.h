#ifndef DIALOG_NEUEBUCHUNG_H
#define DIALOG_NEUEBUCHUNG_H

#include <QDialog>
using namespace std;
namespace Ui {
class Dialog_NeueBuchung;
}

class Dialog_NeueBuchung : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_NeueBuchung(QWidget *parent = nullptr);
    ~Dialog_NeueBuchung();

    double getPrice() const;

    char getTyp() const;

    long getTravelID() const;

    string getStartDatum() const;

    string getEndDatum() const;

    vector<string> getDetails() const;

private slots:
    void on_comboBox_currentIndexChanged(int index);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Dialog_NeueBuchung *ui;
    double price;
    char typ;
    long travelID;
    string startDatum;
    string endDatum;
    vector<string> Details;

};

#endif // DIALOG_NEUEBUCHUNG_H
