#include "dialog_neuebuchung.h"
#include "ui_dialog_neuebuchung.h"

Dialog_NeueBuchung::Dialog_NeueBuchung(QWidget *parent) :           //Konstruktor des PopUps
    QDialog(parent),
    ui(new Ui::Dialog_NeueBuchung)
{
    ui->setupUi(this);
    this->setWindowFlags(((windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowCloseButtonHint & ~Qt::WindowContextHelpButtonHint));
    ui->spinBox->setRange(1,19);                                    //Setzt das Spektrum der Spin box(1-19)
    ui->doubleSpinBox->setValue(000.0);                              //Setzt einen Preis in der double_SpinBox
    ui->comboBox_2->show();
    QStringList ErweiterteDetails;
    ErweiterteDetails<<"Fentster"<<"Gang";
    ui->comboBox_2->addItems(ErweiterteDetails);
    ui->label_6->setText("StartFlughafen");                         //Setzt das 1Text Label
    ui->label_7->setText("Zielflughafen");                          //Setzt das 2Text label
    ui->label_8->setText("Fluggeselschaft");                        //Setzt das 3Text label
    ui->label_10->setText("Sitztyp");
    typ='F';                                                        //setzt den default typ auf F
}
/**
 * @brief Dialog_NeueBuchung::~Dialog_NeueBuchung
 */

Dialog_NeueBuchung::~Dialog_NeueBuchung()                           //Desttuktor
{
    delete ui;
}
/**
 * @brief Dialog_NeueBuchung::on_comboBox_currentIndexChanged Setzt die einzelnen eingeabefelder und Ihre Beschriftung je nachdem
 * was in der Combo Box ausgewähl ist um die details der Buchung korrekt einzulesen
 * @param index
 */

void Dialog_NeueBuchung::on_comboBox_currentIndexChanged(int index)
{
    switch(index){                                              //Switch nach index der Combo Box
    case 0:{
        ui->comboBox_2->clear();
        ui->label_8->show();                                    //label wird angezeigt falls es bei case 2 versteckt wurde
        ui->lineEdit_3->show();                                 //Beschriftung wird angezeigt falls es bei case 2 versteckt wurde
        ui->label_6->setText("StartFlughafen");                 //Setzt den @label_6 text
        ui->label_7->setText("Zielflughafen");                  //Setzt den @Label_7 text
        ui->label_8->setText("Fluggeselschaft");                //Setzt den @Label_8 text
        QStringList ErweiterteDetails;
        ErweiterteDetails<<"Fentster"<<"Gang";
        ui->comboBox_2->addItems(ErweiterteDetails);
        ui->label_10->setText("Sitztyp");
        typ='F';                                                //Setzt den Buchungstyp
        break;
    }
    case 1:{
        ui->comboBox_2->clear();
        ui->label_8->show();                                    //label wird angezeigt falls es bei case 2 versteckt wurde
        ui->lineEdit_3->show();                                 //Beschriftung wird angezeigt falls es bei case 2 versteckt wurde
        ui->label_6->setText("Abholstation");                   //Setzt den @label_6 text
        ui->label_7->setText("Rückgabestation");                //Setzt den @label_7 text
        ui->label_8->setText("Autovermietung");                 //Setzt den @label_8 text
        QStringList ErweiterteDetails;
        ErweiterteDetails<<"Vollkasko"<<"Haftpflicht"<<"Personenschutz"<<"Diebstahlschutz";
        ui->comboBox_2->addItems(ErweiterteDetails);
        ui->label_10->setText("Versicherngstyp");
        typ='R';                                                //Setzt den Buchungstyp
        break;
    }
    case 2:{
        ui->comboBox_2->clear();
        ui->label_6->setText("Hotel");                          //Setzt den @label_6 text
        ui->label_7->setText("Stadt");                          //Setzt den @label_7 text
        ui->label_8->hide();                                    //Verteckt den label_8 text
        ui->lineEdit_3->hide();                                 //verteckt das Line_edit 3
        QStringList ErweiterteDetails;
        ErweiterteDetails<<"NichtRaucher"<<"Raucher";
        ui->comboBox_2->addItems(ErweiterteDetails);
        ui->label_10->setText("Zimmertyp");
        typ='H';                                                //Setzt den Buchungstyp
        break;
    }
    }
}
/**
 * @brief Dialog_NeueBuchung::on_pushButton_clicked Speichert die eingaben des Benutzers damit sie im MainWindow abgefragt werden
 * können
 */

void Dialog_NeueBuchung::on_pushButton_clicked()
{
   price=ui->doubleSpinBox->value();
   travelID=ui->spinBox->value();
   QString fromDate=ui->dateEdit->date().toString("yyyyMMdd");
   QString toDate=ui->dateEdit_2->date().toString("yyyyMMdd");
   startDatum=fromDate.toStdString();
   endDatum=toDate.toStdString();

  switch(typ){
   case 'F':{
       Details.push_back(ui->lineEdit->text().toStdString());
       Details.push_back(ui->lineEdit_2->text().toStdString());
       Details.push_back(ui->lineEdit_3->text().toStdString());
       if(ui->comboBox_2->currentIndex()==0){
           Details.push_back("W");
       }else{
           Details.push_back("A");
       }
       break;
       }
   case 'R':{
      Details.push_back(ui->lineEdit->text().toStdString());
      Details.push_back(ui->lineEdit_2->text().toStdString());
      Details.push_back(ui->lineEdit_3->text().toStdString());
      Details.push_back(ui->comboBox_2->currentText().toStdString());
       break;
   }
   case 'H':{
      Details.push_back(ui->lineEdit->text().toStdString());
      Details.push_back(ui->lineEdit_2->text().toStdString());
      if(ui->comboBox_2->currentIndex()==0){
          Details.push_back("0");
      }else{
          Details.push_back("1");
      }
       break;
   }
   }

   this->hide();
}
/**
 * @brief Dialog_NeueBuchung::getDetails
 * @return Vector Details
 */
vector<string> Dialog_NeueBuchung::getDetails() const
{
    return Details;
}
/**
 * @brief Dialog_NeueBuchung::getEndDatum
 * @return string endDatum
 */
string Dialog_NeueBuchung::getEndDatum() const
{
    return endDatum;
}
/**
 * @brief Dialog_NeueBuchung::getStartDatum
 * @return string startDatum
 */
string Dialog_NeueBuchung::getStartDatum() const
{
    return startDatum;
}
/**
 * @brief Dialog_NeueBuchung::getTravelID
 * @return  long travelID
 */
long Dialog_NeueBuchung::getTravelID() const
{
    return travelID;
}
/**
 * @brief Dialog_NeueBuchung::getTyp
 * @return char Typ
 */
char Dialog_NeueBuchung::getTyp() const
{
    return typ;
}
/**
 * @brief Dialog_NeueBuchung::getPrice
 * @return  double price
 */
double Dialog_NeueBuchung::getPrice() const
{
    return price;
}
/**
 * @brief Dialog_NeueBuchung::on_pushButton_2_clicked wenn abbruch gedrückt wird schreibt die Funktion in Details[1] den wert Abbruch
 */
void Dialog_NeueBuchung::on_pushButton_2_clicked()
{
    Details.push_back("Abbruch");
    reject();
}
