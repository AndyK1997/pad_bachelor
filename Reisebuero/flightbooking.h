#ifndef FLIGHTBOOKING_H
#define FLIGHTBOOKING_H
#include "Booking.h"
using namespace std;
class FlightBooking :public Booking
{
public:
    FlightBooking(long id, double price, long travelID, string fromDate, string toDate,string fromDest, string toDest,string airline, char seatPref );
    ~FlightBooking();
    virtual string Eigenschaft1();
    virtual string Eigenschaft2();
    virtual string Eigenschaft3();
    virtual string Eigenschaft4();
private:
    string fromDest;
    string toDest;
    string airline;
    char seatPref;
};



#endif // FLIGHTBOOKING_H
