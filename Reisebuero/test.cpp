#include "test.h"

Test::Test(QObject *parent) : QObject(parent)
{
    TravelAgency travelagency;
    travelagency.readFile();
}
/**
 * @brief Test::TestFunktion
 */
void Test::TestFunktion(){
    TravelAgency travelagency;
    travelagency.readFile();
    QCOMPARE(travelagency.getTest1('R'), 5);    //vergleicht ob das ergebnids der funktion ==5 ist
    QCOMPARE(travelagency.getTest1('F'), 3);    //vergleicht ob das ergebnis der funktion ==3 ist
    QCOMPARE(travelagency.getTest1('T'), 31);   //vergleicht ob das ergebnis der funktion ==31 ist

}

