#include "hotelbooking.h"

#include "HotelBooking.h"


HotelBooking::HotelBooking(long id, double price, long travelID, string fromDate, string toDate, string hotel, string town, bool Smoke)
{
    this->id = id;
    this->price = price;
    this->travelID = travelID;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->hotel = hotel;
    this->town = town;
    this->smoke=Smoke;
}


HotelBooking::~HotelBooking()
{
}

string HotelBooking::Eigenschaft1() {
    return hotel;
}

string HotelBooking::Eigenschaft2() {
    return town;
}

string HotelBooking::Eigenschaft4(){
    if(smoke==true)
        return "1";
    else {
        return "0";
    }
}


