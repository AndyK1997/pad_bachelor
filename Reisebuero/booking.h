#ifndef BOOKING_H
#define BOOKING_H
#include <string>
using namespace std;
class Booking
{
public:
    Booking();
    ~Booking();
    long getID();
    long getTravID();
    double getPrice();

    virtual string Eigenschaft1()=0;
    virtual string Eigenschaft2();
    virtual string Eigenschaft3();
    virtual string Eigenschaft4();
    string getFromDate() const;

    string getToDate() const;

protected:
    long id;
    double price;
    long travelID;
    string fromDate;
    string toDate;
};



#endif // BOOKING_H
